## Authentication Data
Getting logged in user data:

    private final UserService userService;
    @RequestMapping("/test")
    public String test(Authentication auth) {
        User user = userService.findUserByEmail(auth.getName());
        // ...
    }

Checking if user is admin:

    @RequestMapping("/test")
    public String test(Authentication auth) {
        boolean isAdmin = AuthorityUtils.authorityListToSet(auth.getAuthorities()).contains("ADMIN");
        // ...
    }

## Logging
To use Sl4j logging add the *@Slf4j* annotation above the class in which you want to use the logger. Example:
      
    import lombok.extern.slf4j.Slf4j;
    @Slf4j
    public class LogExample {
        // ...
    }

You can then use different levels of logging:

    log.trace("Trace Message!");
    log.debug("Debug Message!");
    log.info("Info Message!");
    log.warn("Warn Message!");
    log.error("Error Message!");

Log messages will show in the console.

## Mail Service
To use the Mail Service in a class insert it via dependency injection:

    @RequiredArgsConstructor
    public class Example {
        private final MailService mailService;
        // ...
    }

Sending mail is done using the following methods:

    sendMessage(String to, String subject, String body);
    sendMessage(String to, String subject, String body, String pathToAttachment);
    sendMessage(SimpleMailMessage message);
    sendMessage(MimeMessage message);

## Building sass and typescript
```
    npm i
    gulp
```