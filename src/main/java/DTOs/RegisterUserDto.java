package DTOs;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import Validations.PasswordMatches;
import Validations.ValidEmail;


@Valid
@PasswordMatches
public class RegisterUserDto {

	@NotNull
	@NotEmpty
//	@ValidEmail
	private String email;

	@NotNull
	@NotEmpty
	private String name;

	@NotNull
	@NotEmpty
	@Size(min = 6, max = 30, message = "Password must be between 6 and 30 characters long")
	private String password;

	@NotNull
	@NotEmpty
	private String confirmPassword;
	
	@NotNull
	@NotEmpty
	private String location;

	public RegisterUserDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
}
