package com.sportmanager.SportManager.RestControllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sportmanager.SportManager.Models.EventType;
import com.sportmanager.SportManager.Services.EventTypeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/event-types")
@RequiredArgsConstructor
public class EventTypeRestController {
	private final EventTypeService eventTypeService;

	@GetMapping
	public ResponseEntity<List<EventType>> findAll() {
		return ResponseEntity.ok(eventTypeService.findAll());
	}
}
