package com.sportmanager.SportManager.RestControllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UsersRestController {

	private final UserService userService;

	@GetMapping
	public ResponseEntity<List<User>> findAll() {
		return ResponseEntity.ok(userService.findAll());
	}

	@GetMapping(path = { "/{id}" })
	public ResponseEntity<User> findById(@PathVariable long id) {
		return userService.findById(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	@GetMapping(path = { "/subscriptions/{id}" })
	public ResponseEntity<List<Long>> listSubscriptions(@PathVariable long id) {
		return ResponseEntity.ok(userService.listSubscriptions(id));

		/*
		 * userService.listSubscriptions(id).map(record ->
		 * ResponseEntity.ok().body(record)) .orElse(ResponseEntity.notFound().build());
		 */
	}

	@PostMapping(path = { "/subscriptions/{id}" })
	public ResponseEntity<Boolean> toggleSubscription(@PathVariable long id,
			@RequestBody RequestParams subscriptionDetails) {

		return ResponseEntity.ok(userService.toggleSubscription(id, subscriptionDetails.getEventTypeId()));
	}

	@GetMapping(path = "/current")
	public ResponseEntity<User> findCurrent(Authentication auth) {
		if (!auth.isAuthenticated())
			return ResponseEntity.notFound().build();

		return ResponseEntity.ok().body(userService.findUserByEmail(auth.getName()));
	}

	@PostMapping
	public User create(@RequestBody User user) {
		return userService.save(user);
	}

	
	
	@PostMapping(value = "/changePassword/{id}")
	public ResponseEntity<Boolean> changePassword(@PathVariable long id, @RequestBody RequestParams passwordDetails) {
		
		if (passwordDetails.getPassword().length()<6)
		{
			return ResponseEntity.ok(false);
		}
		else
		{
			return ResponseEntity.ok(userService.changePassword(id, passwordDetails.getPassword()));
		}
		
	}
	
	@PostMapping(value = "/changeAvatar/{id}")
	public ResponseEntity<Boolean> changeAvatar(@PathVariable long id, @RequestBody RequestParams avatarDetails) {
		if (avatarDetails.getAvatarId() <= 30 && avatarDetails.getAvatarId() > 0)
			return ResponseEntity.ok(userService.changeAvatar(id, avatarDetails.getAvatarId()));
		else
			return ResponseEntity.ok(false);
	}

	@PostMapping(value = "/changeCity/{id}")
	public ResponseEntity<Boolean> changeCity(@PathVariable long id, @RequestBody RequestParams avatarDetails) {
			return ResponseEntity.ok(userService.changeCity(id, avatarDetails.getCity()));
	}
	
	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		return userService.findById(id).map(record -> {
			userService.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}

}

// separate model??
class RequestParams {
	private String city;
	private int avatarId;
	private Long userId;
	private Long eventTypeId;
	private String password;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(Long eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAvatarId() {
		return avatarId;
	}

	public void setAvatarId(int avatarId) {
		this.avatarId = avatarId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
