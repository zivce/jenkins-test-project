package com.sportmanager.SportManager.Controllers;

import java.util.HashSet;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.sportmanager.SportManager.Models.ConfirmationToken;
import com.sportmanager.SportManager.Models.Role;
import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.ConfirmationTokenRepository;
import com.sportmanager.SportManager.Services.MailService;
import com.sportmanager.SportManager.Services.RoleRepository;
import com.sportmanager.SportManager.Services.UserService;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class ClientRegisterController {
	
	private final UserService userService;
	private final ConfirmationTokenRepository confirmationTokenRepository;
	private final RoleRepository roleRepository;
	
	
	@GetMapping(path = "client/confirm-register")
	public String register(@RequestParam("token") String confirmationToken, Model model) {
		
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token != null)
        {
            User user = userService.findUserByEmail(token.getUser().getEmail());
            Role role = roleRepository.findByRole("USER");
    		HashSet<Role> roles = new HashSet<Role>();
    		roles.add(role);
    		user.setRoles(roles);
            user.setConfirmed(true);
            userService.save(user);
            
            confirmationTokenRepository.delete(token);
            
            model.addAttribute("registerSuccess", true);
        }
        else
        {
        	model.addAttribute("registerSuccess", false);

        }
        
    	String confirmRegisterUrl = "/confirm-register"; 
    	return confirmRegisterUrl;
	}
}
