package com.sportmanager.SportManager.Controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequiredArgsConstructor
public class AdminController {

	private final UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {

		// Redirect if logged in
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			User user = userService.findUserByEmail(auth.getName());
			model.addAttribute("userId", user.getId());
			return "event-calendar";
		}

		if (error != null) {
			model.addAttribute("errorEmail", "");
			model.addAttribute("errorPassword", "Your email or password is invalid.");
		}
		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");
		return "login";
	}

	@GetMapping("/admin")
	public String welcome(Model model, Authentication auth) {

		User user = userService.findUserByEmail(auth.getName());
		model.addAttribute("userId", user.getId());
		return "event-calendar";
	}

}
