package com.sportmanager.SportManager.Controllers;

import com.sportmanager.SportManager.Models.*;
import com.sportmanager.SportManager.Services.MailService;
import com.sportmanager.SportManager.Services.UserService;

import lombok.RequiredArgsConstructor;

import java.security.Principal;
import java.util.*;

import javax.annotation.PostConstruct;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/admin/users-control-panel")
public class UsersControlPanelController {

	private final UserService userService;
	private final MailService mailService;

	@RequestMapping(value = "/username", method = RequestMethod.GET)
	@ResponseBody
	public String currentUserName(Principal principal) {
		return principal.getName();
	}

	@GetMapping
	public String showUsers(Model model, Principal principal,
			@RequestParam(value = "p", required = false) Integer page) {

		if (page == null)
			page = 1;

		int resultsPerPage = 10;
		int totalNumberOfResults = userService.countUsers();
		int numberOfPages = totalNumberOfResults / resultsPerPage + 1;

		List<User> usersList = userService.findByPage(page, resultsPerPage);

		model.addAttribute("usersList", usersList);
		model.addAttribute("numberOfPages", numberOfPages);
		model.addAttribute("totalNumberOfResults", totalNumberOfResults);
		model.addAttribute("resultsPerPage", resultsPerPage);
		model.addAttribute("currentPage", page);
		model.addAttribute("loggedInUserEmail", principal.getName());

		return "users-control-panel";
	}

	@PostMapping(value = "/delete")
	public RedirectView deleteUser(@RequestParam(value = "deleteUserId", required = true) String deleteUserId,
			Model model) {

		long id = Long.parseLong(deleteUserId);

		if (userService.findById(id).isPresent()) {
			userService.deleteById(id);
		}

		String usersListUrl = "/admin/users-control-panel";
		return new RedirectView(usersListUrl);
	}

	@PostMapping(value = "/upgrade")
	public RedirectView upgradeUser(@RequestParam(value = "upgradeUserId", required = true) String upgradeUserId,
			Model model) throws InterruptedException {

		long id = Long.parseLong(upgradeUserId);

		if (userService.upgradeUser(id)) {
			User user = userService.findById(id).get();
			mailService.sendMessage(user.getEmail(), "EventManager account upgrade information",
					"Your account has been upgraded to administrator.");
		}

		String usersListUrl = "/admin/users-control-panel";
		return new RedirectView(usersListUrl);

	}

	@PostMapping(value = "/downgrade")
	public RedirectView downgradeUser(@RequestParam(value = "downgradeUserId", required = true) String downgradeUserId,
			Model model) throws InterruptedException {

		long id = Long.parseLong(downgradeUserId);

		if (userService.downgradeUser(id)) {
			User user = userService.findById(id).get();
			mailService.sendMessage(user.getEmail(), "EventManager account downgrade information",
					"Your account no longer has administrator privilages.");
		}

		String usersListUrl = "/admin/users-control-panel";
		return new RedirectView(usersListUrl);
	}

}
