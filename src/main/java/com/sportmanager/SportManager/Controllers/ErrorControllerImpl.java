package com.sportmanager.SportManager.Controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorControllerImpl implements ErrorController {

	@RequestMapping("/error")
	public String handleError(HttpServletRequest httpRequest, Model model) {

		int errorCode = getErrorCode(httpRequest);
		String errorMsg = "";
		String errorDescription = "";

		switch (errorCode) {
		case 400: {
			errorMsg = "Bad Request";
			errorDescription = "The server could not process your request";
			break;
		}
		case 401: {
			errorMsg = "Unauthorized Access";
			errorDescription = "You haven't provided valid authentication credentials";
			break;
		}
		case 403: {
			errorMsg = "Forbidden Access";
			errorDescription = "You do not have the necessary permissions to access the resource";
			break;
		}
		case 404: {
			errorMsg = "Resource Not Found";
			errorDescription = "The requested resource has not been found";
			break;
		}
		case 500: {
			errorMsg = "Internal Server Error";
			errorDescription = "The server could not process your request";
			break;
		}
		case 503: {
			errorMsg = "Service Unavailable";
			errorDescription = "The server cannot handle your request. Try again later.";
			break;
		}
		}

		model.addAttribute("code", errorCode);
		model.addAttribute("message", errorMsg);
		model.addAttribute("description", errorDescription);
		return "error";
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}

	private int getErrorCode(HttpServletRequest httpRequest) {
		return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
	}

}
