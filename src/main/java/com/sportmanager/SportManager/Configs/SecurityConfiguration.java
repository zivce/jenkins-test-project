package com.sportmanager.SportManager.Configs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	private final DataSource dataSource;
	private final AuthenticationSuccessHandler authHandler;

	private static String[] PUBLIC_URLS = new String[] { "/login", "/register", "/register.html", "/logout",
			"/webjars/**", "/resetPassword", "/messagePassword", "/client/confirm-register", "/user/resetPassword",
			"/forgotPassword", "/h2-console/**", "/api/**", "/csrf-token" };

	@Value("${spring.queries.users-query}")
	private String usersQuery;

	@Value("${spring.queries.roles-query}")
	private String rolesQuery;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery)
				.dataSource(dataSource).passwordEncoder(bCryptPasswordEncoder);
	}

	@Bean
	public AuthenticationSuccessHandler myAuthHandler() {
		return new CustomAuthenticationSuccessHandler();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers(PUBLIC_URLS).permitAll().antMatchers("/api/**")
				.hasAnyAuthority("ADMIN", "USER").antMatchers("/admin/**").hasAuthority("ADMIN").anyRequest()
				.authenticated().and().formLogin().loginPage("/login").successHandler(myAuthHandler())
				.loginProcessingUrl("/doLogin").failureUrl("/login?error=true").usernameParameter("email")
				.passwordParameter("password").and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/login");

		http.csrf().ignoringAntMatchers(PUBLIC_URLS);
		http.headers().frameOptions().disable();
		// .and().logout()
		// .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		// .logoutSuccessUrl("/").and().exceptionHandling()
		// .accessDeniedPage("/access-denied");
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/admin_root/**", "/js/**", "/styles/**", "/static/**", "/dist/**",
				"/css/**", "/js/**", "/images/**");
	}

}