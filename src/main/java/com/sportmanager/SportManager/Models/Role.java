package com.sportmanager.SportManager.Models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "roles")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "role")
	private String role;

	@JsonBackReference
	@ManyToMany(targetEntity = User.class, mappedBy = "roles")
	private List<User> users;

	@PreRemove
	private void removeRoleFromUsers() {
		for (User u : users) {
			u.getRoles().remove(this);
		}
	}
}
