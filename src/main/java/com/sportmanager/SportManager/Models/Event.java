package com.sportmanager.SportManager.Models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "events")
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	private int numberOfParticipants;

	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "event_type_id")
	private EventType type;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "owner_id")
	private User owner;

	@JsonBackReference
	@ManyToMany(targetEntity = User.class, mappedBy = "participatingIn")
	private List<User> participatingUsers;

	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
	private Date updatedAt;

	@PreRemove
	private void removeParticipatingUsers() {
		for (User u : participatingUsers) {
			u.getParticipatingIn().remove(this);
		}
	}

}
