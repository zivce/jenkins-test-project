package com.sportmanager.SportManager.Services;

import org.springframework.data.repository.CrudRepository;

import com.sportmanager.SportManager.Models.ConfirmationToken;

public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, String> {  
    ConfirmationToken findByConfirmationToken(String confirmationToken);
}
