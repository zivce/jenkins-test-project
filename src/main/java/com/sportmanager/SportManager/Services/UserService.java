package com.sportmanager.SportManager.Services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sportmanager.SportManager.Models.Event;
import com.sportmanager.SportManager.Models.EventType;
import com.sportmanager.SportManager.Models.PasswordResetToken;
import com.sportmanager.SportManager.Models.Role;
import com.sportmanager.SportManager.Models.User;

import DTOs.RegisterUserDto;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

	private final UserRepository userRepository;
	private final EventRepository eventRepository;
	private final PasswordResetTokenRepository passwordTokenRepository;
	private final RoleRepository roleRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final EventTypeRepository eventTypeRepository;
	@Autowired
	MailService mS;


	public List<User> findAll() {
		return userRepository.findAll();
	}

	public Optional<User> findById(Long id) {
		return userRepository.findById(id);
	}

	public User save(User user) {
		// user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		// user.setConfirmed(true);
		// Role userRole = roleRepository.findByRole("ADMIN");
		// user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		return userRepository.save(user);
	}

	public void deleteById(Long id) {

		userRepository.deleteById(id);
	}

	public void createPasswordResetTokenForUser(User user, String token) {
		PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordTokenRepository.save(myToken);
	}

	public boolean upgradeUser(Long id) {

		Optional<User> userOptional = userRepository.findById(id);
		if (userOptional.isPresent()) {
			User user = userOptional.get();
			Role role = roleRepository.findByRole("ADMIN");
			Set<Role> roles = user.getRoles();
			if (!roles.add(role)) {
				return false;
			}
			roles.remove(roleRepository.findByRole("USER"));
			user.setRoles(roles);
			userRepository.save(user);
			return true;
		}
		return false;
	}

	public boolean downgradeUser(Long id) {

		Optional<User> userOptional = userRepository.findById(id);
		if (userOptional.isPresent()) {
			User user = userOptional.get();
			Role role = roleRepository.findByRole("ADMIN");
			Set<Role> roles = user.getRoles();
			if (!roles.remove(role)) {
				return false;
			}
			roles.add(roleRepository.findByRole("USER"));
			user.setRoles(roles);
			userRepository.save(user);
			return true;
		}
		return false;
	}

	public boolean toggleParticipatingIn(Long userId, Long eventId) {

		Optional<User> userOptional = userRepository.findById(userId);
		Optional<Event> eventOptional = eventRepository.findById(eventId);
		if (userOptional.isPresent() && eventOptional.isPresent()) {

			User user = userOptional.get();
			Event event = eventOptional.get();

			List<Event> participatingIn = user.getParticipatingIn();
			if (participatingIn.contains(event)) {
				participatingIn.remove(event);
			} else {
				// Check if maximum participant number is more than users participating
				if (event.getNumberOfParticipants() <= event.getParticipatingUsers().size())
					return false;
				participatingIn.add(event);
			}

			user.setParticipatingIn(participatingIn);
			userRepository.save(user);
			return true;
		}
		return false;
	}

	public User findUserByEmail(final String email) {
		return userRepository.findByEmail(email);
	}

	public String validatePasswordResetToken(long id, String token) {

		final PasswordResetToken passToken = passwordTokenRepository.findByToken(token);
		if ((passToken == null) || (passToken.getUser().getId() != id)) {
			return "invalidToken";
		}

		final Calendar cal = Calendar.getInstance();
		if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return "expired";
		}

		return "confirmed";
	}

	public User registerNewUserAccount(RegisterUserDto registerUserDTO) {

		User user = new User();
		user.setEmail(registerUserDTO.getEmail());
		user.setPassword(bCryptPasswordEncoder.encode(registerUserDTO.getPassword()));
		user.setConfirmed(false);
		
		Random r = new Random();
		int min=1;
		int max=30;
		int temp=r.nextInt((max - min) + 1);
		temp+=min;
		user.setAvatarId(temp);
		
		user.setName(registerUserDTO.getName());
		user.setLocation(registerUserDTO.getLocation());
		return save(user);
	}

	private boolean emailExists(String email) {
		User user = findUserByEmail(email);
		if (user != null) {
			return true;
		}
		return false;
	}

	public List<User> findByPage(int pageNumber, int usersPerPage) {

		Pageable pageable = PageRequest.of(pageNumber - 1, usersPerPage);

		List<User> usersList = userRepository.findAll(pageable).getContent();

		return usersList;
	}

	public List<Long> listSubscriptions(Long id) {
		List<EventType> allEventTypes = eventTypeRepository.findAll();
		// za svaki event type ispitaj da li je taj id subscribeovan na njega
		List<Long> listOfSubscriptions = new ArrayList<>();

		for (EventType eventType : allEventTypes) {
			List<User> users = eventType.getSubscribedUsers();
			for (User user : users) {
				if (id == user.getId()) {
					listOfSubscriptions.add(eventType.getId());
				}
			}
		}

		return listOfSubscriptions;

	}
	
	public boolean changeCity(Long userID, String city) {
		
		Optional<User> userOptional = userRepository.findById(userID);
		
		if (userOptional.isPresent()) {
			if (city.equals("Nis")||city.equals("Belgrade")||city.equals("Kragujevac")) {
			User user = userOptional.get();
			user.setLocation(city);
			userRepository.save(user);
			System.out.println(city);
			return true;
			}
			else {
				
				return false;
			}
		}
		else {return false;}
	}
	public boolean changePassword(Long userID, String password) {
		
		Optional<User> userOptional = userRepository.findById(userID);
		if (userOptional.isPresent()) {
			
			User user = userOptional.get();
			user.setPassword(bCryptPasswordEncoder.encode(password));
			userRepository.save(user);
			return true;
		}
		else return false;
	}
	public boolean toggleSubscription(Long userId, Long eventTypeId) {
		boolean inserted = false;
		Optional<User> userOptional = userRepository.findById(userId);
		Optional<EventType> eventTypeOptional = eventTypeRepository.findById(eventTypeId);
		if (userOptional.isPresent() && eventTypeOptional.isPresent()) {
			User user = userOptional.get();
			EventType eventType = eventTypeOptional.get();

			List<EventType> subscribedTo = user.getSubscribedTo();

			if (subscribedTo.contains(eventType)) {

				subscribedTo.remove(eventType);

			} else {
				subscribedTo.add(eventType);
				inserted = true;
			}

			user.setSubscribedTo(subscribedTo);
			userRepository.save(user);
			if (inserted)
				try {
					mS.sendMessage(user.getEmail(), "Succesfuly subscribed to " + eventType.getName(),
							"You have succesfully subscribed to " + eventType.getName()
									+ " events. From now on you will be notified by mail whenever there is a place at a new event. Enjoy your day!");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else {
				try {
					mS.sendMessage(user.getEmail(), "Unsubscribed from " + eventType.getName(),
							"You have succesfully unsubscribed from " + eventType.getName()
									+ " events. From now on, you won't be receiving mail notifications when there is a new event of this kind. Enjoy your day!");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return true;
		} else
			return false;
	}

	public boolean changeAvatar(Long id, int avatarId) {
		Optional<User> userOptional = userRepository.findById(id);
		if (userOptional.isPresent()) {
			User user = userOptional.get();
			user.setAvatarId(avatarId);
			userRepository.save(user);
		}
		return true;
	}

	public int countUsers() {
		return (int) userRepository.count();
	}
}
