package com.sportmanager.SportManager.Services;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sportmanager.SportManager.Models.EventType;
import com.sportmanager.SportManager.Models.User;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Long> {
	Optional<EventType> findByName(String name);
}
