package com.sportmanager.SportManager.Services;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class MailService {

	private final JavaMailSender mailSender;

	@Async("mailExecutor")
	public void sendMessage(String to, String subject, String body) throws InterruptedException {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject);
		message.setText(body);
		mailSender.send(message);
	}

	@Async("mailExecutor")
	public void sendMessage(SimpleMailMessage message) throws InterruptedException {
		mailSender.send(message);
	}

	@Async("mailExecutor")
	public void sendMessage(String to, String subject, String body, String pathToAttachment)
			throws MessagingException, InterruptedException {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		helper.setTo(to);
		helper.setSubject(subject);
		helper.setText(body);

		FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
		helper.addAttachment("Attachment", file);

		mailSender.send(message);

	}

	@Async("mailExecutor")
	public void sendMessage(MimeMessage message) throws MessagingException, InterruptedException {
		mailSender.send(message);
	}
}
