package com.sportmanager.SportManager.Services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import com.sportmanager.SportManager.Models.EventType;
import com.sportmanager.SportManager.Models.User;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class EventTypeService {
	private final EventTypeRepository eventTypeRepository;

	public List<EventType> findAll() {
		return eventTypeRepository.findAll();
	}
	
	public Optional<EventType> findById(Long id) {
		return eventTypeRepository.findById(id);
	}
	
	public Optional<EventType> findByName(String name) {
		return eventTypeRepository.findByName(name);
	}
	
	public EventType save(EventType eventType) {
		return eventTypeRepository.save(eventType);
	}

	public void deleteById(Long id) {
			eventTypeRepository.deleteById(id);	
	}
	
	public List<EventType> findByPage(int pageNumber, int resultsPerPage) {
		
		Pageable pageable = PageRequest.of(pageNumber-1, resultsPerPage);
		
		List<EventType> usersList = eventTypeRepository.findAll(pageable).getContent();
	
		return usersList;
	}
	
	public int countEventTypes() {
		return (int) eventTypeRepository.count();
	}

}
