function acceptDelete() {
		return confirm("Are you sure you want to delete user?");
	}

	function acceptUpgrade() {
		return confirm("Are you sure you want to upgrade user?");
	}

	function acceptDowngrade() {
		return confirm("Are you sure you want to downgrade user?");
	}
