const nameField: HTMLInputElement = <HTMLInputElement>document.getElementById('name');
const minPartField: HTMLInputElement = <HTMLInputElement>document.getElementById('minPart');
const maxPartField: HTMLInputElement = <HTMLInputElement>document.getElementById('maxPart');
const maxDuration: HTMLInputElement = <HTMLInputElement>document.getElementById('maxDuration');

const eventTypeForm: HTMLElement = document.getElementById("eventTypeForm");
eventTypeForm.onsubmit = (e) => {
	const valid = validation();
	if(!valid){
		e.preventDefault();
	}
	return valid;
}

function validation() {

	const nameCorrect: boolean = !checkIfEmpty(nameField);
	const minParticipantsCorrect: boolean = !checkIfEmpty(minPartField);
	const maxParticipantsCorrect: boolean = !checkIfEmpty(maxPartField);
	const maxDurationCorrect: boolean = !checkIfEmpty(maxDuration);

	const minParticipantsRangeCorrect: boolean = !checkRange(minPartField);
	const maxParticipantsRangeCorrect: boolean = !checkRange(maxPartField);
	const maxDurationRangeCorrect: boolean = !checkRange(maxDuration);
	
	let formPassedValidation = true;

	formPassedValidation = formPassedValidation && nameCorrect;
	formPassedValidation = formPassedValidation && minParticipantsCorrect;
	formPassedValidation = formPassedValidation && maxParticipantsCorrect;
	formPassedValidation = formPassedValidation && maxDurationCorrect;
	formPassedValidation = formPassedValidation && minParticipantsRangeCorrect;
	formPassedValidation = formPassedValidation && maxParticipantsRangeCorrect;
	formPassedValidation = formPassedValidation && maxDurationRangeCorrect;

	return formPassedValidation;
}


function checkRange(field: HTMLInputElement) {
	let rangeCorrect = true;
	switch (field) {
		case minPartField:
		case maxDuration:
			rangeCorrect = rangeCorrect && !(field.valueAsNumber < 1);
			break;
		case maxPartField:
			rangeCorrect = rangeCorrect && !(field.valueAsNumber > 300);
			rangeCorrect = rangeCorrect && !(field.valueAsNumber < 1);
			rangeCorrect = rangeCorrect && !(field.valueAsNumber < minPartField.valueAsNumber);
			break;	
	}

	if (rangeCorrect) {
		setValid(field);
		return false;
	} else {
		setInvalid(field, `Please enter valid ${field.name}`);
		return true;
	}
}

function checkIfEmpty(field) {
	if (isEmpty(field.value.trim())) {
		setInvalid(field, `Please enter valid ${field.name}`);
		return true;
	} else {
		setValid(field);
		return false;
	}
}

function isEmpty(value) {
	return value === '';
}

function setInvalid(field, message) {
	field.parentNode.className = "form-group has-error";
	field.nextElementSibling.className = "help-block with-errors";
	field.nextElementSibling.innerHTML = message;

}

function setValid(field) {
	field.nextElementSibling.innerHTML = '';
	field.parentNode.className = "form-group has-success";
	field.nextElementSibling.className = "help-block";

}
