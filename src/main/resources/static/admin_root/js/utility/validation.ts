export default class Validation {
    public static showErrors (form, errors) {
        form.querySelectorAll('.form-control').forEach((function(element) {
            Validation.showErrorForInput(element, errors[element.name]);
        }));
    };
    
    private static showErrorForInput(input, error) {
        if (error) {
            Validation.setInvalid(input, error);
        } else {
            Validation.setValid(input);
        }
    };
    
    private static setInvalid(field, message) {
        field.parentNode.className = "form-group has-error";
        field.parentNode.lastElementChild.className = "help-block with-errors";
        field.parentNode.lastElementChild.textContent = message;
    };
    
    private static setValid(field) {
        field.parentNode.className = "form-group";
        field.parentNode.lastElementChild.className = "help-block";
        field.parentNode.lastElementChild.textContent = '';
    };
}