export default interface ModalContext {
    searchUsersInput: HTMLInputElement,
    usersDataList: HTMLDataListElement,
    maxParticipantsNumber: HTMLInputElement,
    endTime: HTMLInputElement,
    participantsList: HTMLUListElement,
    selectedListItem: HTMLLIElement,
    helpBlock: HTMLElement,
    modalForm: HTMLFormElement,
    startTime: HTMLInputElement,
    currentDate: string,
    eventTypeId: HTMLInputElement,
}