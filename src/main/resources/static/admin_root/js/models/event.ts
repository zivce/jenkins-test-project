export default interface Event {
    id: number,
    title: string,
    resourceId: string,
    start: any,
    end: any
}