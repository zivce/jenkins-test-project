export default interface EventTypeResource {
    id: string,
    title: string,
    eventBackgroundColor?: string
}