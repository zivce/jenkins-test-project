import apiJSON from "./api";
import ModalContext from "./models/modal-context";

export default class CalendarHandler {
    public static addEvents(calendar) {
        fetch(apiJSON.EVENTS_URL)
            .then(function (response) {
                return response.json();
            })
            .then(function (jsonData) {
                jsonData.forEach(function (element) {
                    const event = {
                        id: element.id,
                        resourceId: element.typeId,
                        start: element.startDate,
                        end: element.endDate,
                        // Extended props
                        createdAt: element.createdAt,
                        updatedAt: element.updatedAt,
                        numberOfParticipants: element.numberOfParticipants,
                        participatingUsers: element.participatingUsers,
                        ownerId: element.ownerId
                    };
                    calendar.addEvent(event);
                });
            })
            .catch(err => new Error(`Looks like there was a problem. Status Code:${err}`));
    }

    public static selectTimeSlots(selectedTimeSlotsInfo) {
        const limit = selectedTimeSlotsInfo.resource.extendedProps.maxDuration;
        const startTime = new Date(selectedTimeSlotsInfo.start);
        const endTime = new Date(selectedTimeSlotsInfo.end);
        // This will give difference in milliseconds
        const difference = endTime.getTime() - startTime.getTime();
        const resultInMinutes = Math.round(difference / 60000);
        const limitPassed = resultInMinutes > limit;
        return !limitPassed;
    }

    public static fetchUsers() {
        return fetch(apiJSON.USERS_URL)
            .then(function (response) {
                return response.json();
            })
            .then(function (jsonData) {
                const usersFromJson = jsonData.reduce((acc, val) => {
                    acc.push(val);
                    return acc;
                }, [])
                return usersFromJson;
            })
            .catch(err => {
                return new Error(`Looks like there was a problem. Status Code:${err}`)
            })

    }

    public static eventClickHandler(calendarContext, modalContext) {

        // NOTE Bootstrap-jQuery inserted globally!
        // @ts-ignore
        $('#exampleModal').modal();

        CalendarHandler.configureEditModal(calendarContext, modalContext);

        CalendarHandler.fetchParticipants(calendarContext)
            .then((res) => CalendarHandler.createParticipantsView(res, modalContext))
            .catch(err => console.error(err));


        document.getElementById('modalForm').onsubmit = (event) => {
            event.preventDefault();
            CalendarHandler.editEventSubmitHandler(calendarContext);
        }

        document.getElementById('deleteButton').onclick = (event) => {
            CalendarHandler.deleteEventHandler(calendarContext);
        }
    }

    private static configureEditModal(calendarContext, modalContext) {
        const { searchUsersInput, participantsList } = modalContext;

        // Set modal title
        (<HTMLLabelElement>document.getElementById('exampleModalLabel')).textContent = 'Edit Event';

        // Set event type
        (<HTMLInputElement>document.getElementById('eventTypeName')).value = calendarContext.event.getResources()[0].title;

        // Hide search input
        searchUsersInput.classList.add('hidden');

        // Disable number of participants field
        (<HTMLInputElement>document.getElementById('participants')).disabled = true;

        // Set number of participants
        (<HTMLInputElement>document.getElementById('participants')).value = calendarContext.event.extendedProps.numberOfParticipants;

        // Set stat time
        (<HTMLInputElement>document.getElementById('startTime')).value = calendarContext.event.start.toLocaleTimeString('en-GB');
        // Set end time
        (<HTMLInputElement>document.getElementById('endTime')).value = calendarContext.event.end.toLocaleTimeString('en-GB');

        (<HTMLButtonElement>document.getElementById('deleteButton')).classList.remove('hidden');
    }

    private static configureCreateModal(calendarContext, modalContext) {
        const { searchUsersInput } = modalContext;

        // Set modal title
        (<HTMLLabelElement>document.getElementById('exampleModalLabel')).textContent = 'Create an event';

        // Set event type
        (<HTMLInputElement>document.getElementById('eventTypeName')).value = calendarContext.resource.title;

        // Disable number of participants field
        (<HTMLInputElement>document.getElementById('participants')).disabled = false;

        // Set stat time
        (<HTMLInputElement>document.getElementById('startTime')).value = calendarContext.start.toLocaleTimeString('en-GB');

        // Set end time
        (<HTMLInputElement>document.getElementById('endTime')).value = calendarContext.end.toLocaleTimeString('en-GB');

        // Show search input
        searchUsersInput.classList.remove('hidden');

        // Hide delete button
        (<HTMLButtonElement>document.getElementById('deleteButton')).classList.add('hidden');

        // Set button to create event
        (<HTMLButtonElement>document.getElementById('submitButton')).textContent = 'Create';
    }

    private static fetchParticipants(calendarContext) {

        return fetch(apiJSON.EVENTS_URL + calendarContext.event.id + "/participating")
            .then(function (response) {
                return response.json();
            })
            .catch(err => new Error(`Looks like there was a problem. Status Code:${err}`))
    }

    private static createParticipantsView(participants: Array<any>, modalContext: ModalContext) {
        const { participantsList } = modalContext;
        const label = (<HTMLLabelElement>document.getElementById('participantsLabel'));
        label.textContent = 'Going (' + participants.length + ')';

        // Clear participant list
        const participantsArray = Array.from(modalContext.participantsList.children);
        participantsArray.forEach((participant: HTMLElement) => {
            if (!!participant && !!participant.classList) {
                if (participant.classList.contains("event-modal__participant")) {
                    participant.parentNode.removeChild(participant);
                }
            }
        });

        // const participantsList = document.getElementById("participantsList");

        participants.reduce((listOfParticipants, participant) => {

            const li = document.createElement("li");
            li.classList.add('list-group-item');
            li.classList.add('event-modal__participant');
            li.appendChild(document.createTextNode(participant.email));
            listOfParticipants.appendChild(li);
            return listOfParticipants;

        }, participantsList)

        // Find if logged in user is in participants list
        const newArray = Array.from(modalContext.participantsList.children);
        const userInList = !!newArray.find((participant: HTMLElement) => {
            if (!!participant && !!participant.classList) {
                if (participant.classList.contains("event-modal__participant")) {
                    return participant.textContent.indexOf((<any>window).client.email) !== -1;
                }
            }
        });

        (<HTMLButtonElement>document.getElementById('submitButton')).textContent = userInList ? 'Leave' : 'Attend';

    }

    private static editEventSubmitHandler(calendarContext) {
        let editEventData = new FormData();
        editEventData.append('eventId', calendarContext.event.id);
        editEventData.append('userId', (<any>window).client.id);


        fetch(apiJSON.PARTICIPATE_URL, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: editEventData // body data type must match "Content-Type" header
        })
            .then((res) => {
                return res.json();
            })
            .then((jsonRes) => {
                if ((<any>jsonRes).status === "error") {
                    alert((<any>jsonRes).message);
                }
                //@ts-ignore
                $('#exampleModal').modal('hide');
            })
            .catch(err => console.error(err));


        return false;
    }

    private static createEventHandler(calendarContext, modalContext: ModalContext, calendar) {

        const ownerid = (<any>window).client.id;
        const participants = Array.from(modalContext.participantsList.children).reduce((acc, val) => {
            if (val.getAttribute('userId')) {
                acc.push(val.getAttribute('userId'));
            }
            return acc;
        }, []);

        // Create formdata object with values from inputs
        var formData = new FormData();
        formData.append('numberOfParticipants', modalContext.maxParticipantsNumber.value);
        formData.append('startDate', modalContext.startTime.value);
        formData.append('endDate', modalContext.endTime.value);
        formData.append('currentDate', modalContext.currentDate);
        formData.append('eventTypeId', modalContext.eventTypeId.value);
        formData.append('ownerid', ownerid);
        formData.append('participatingUsers', <any>participants);

        // Send post request to api/events
        fetch(apiJSON.EVENTS_URL, {
            method: 'POST',
            body: formData
        })
            .then((res) => {

                if (res.status === 400) {
                    alert("The time slot is already taken!");
                }
                return res.json();
            })
            .then((jsonRes) => {
                // Create event object from result and insert into calendar

                const startD = new Date(jsonRes.startDate);
                const endD = new Date(jsonRes.endDate);

                calendar.addEvent({
                    id: jsonRes.id,
                    resourceId: modalContext.eventTypeId.value,
                    start: startD,
                    end: endD,
                    // Extended props
                    createdAt: jsonRes.createdAt,
                    updatedAt: jsonRes.updatedAt,
                    numberOfParticipants: modalContext.maxParticipantsNumber.value,
                    participatingUsers: participants,
                    ownerId: ownerid,
                });
                //@ts-ignore
                $('#exampleModal').modal('hide');
            })
            .catch(err => {
                return new Error("An error has occured: " + err);
            })
    }

    private static deleteEventHandler(calendarContext) {
        fetch(apiJSON.EVENTS_URL + calendarContext.event.id, {
            method: 'DELETE'
        })
            .then(() => {
                calendarContext.event.remove();
            })
            .catch(err => {
                console.error('removed', err);
            })
    }

    public static fetchResources(fetchInfo, successCallback, failureCallback) {
        fetch(apiJSON.EVENT_TYPES_URL)
            .then(function (response) {
                return response.json();
            })
            .then(function (jsonData) {

                let eventTypes = jsonData.reduce((acc, resource) => {
                    acc.push({
                        id: resource.id,
                        title: resource.name + ' (max ' + resource.maxDuration + 'min)',
                        createdAt: resource.createdAt,
                        updatedAt: resource.updatedAt,
                        maxDuration: resource.maxDuration,
                        minimumParticipants: resource.minimumParticipants,
                        maximumParticipants: resource.maximumParticipants,
                    })
                    return acc;
                }, []);
                successCallback(eventTypes);
            })
            .catch(err => console.error(err));
    }

    public static calendarSelect(calendarContext, modalContext: ModalContext, calendar) {
        CalendarHandler.configureCreateModal(calendarContext, modalContext);

        const eventFull: boolean =
            modalContext.participantsList.childElementCount - 1 >=
            modalContext.maxParticipantsNumber.valueAsNumber;

        if (eventFull) {
            modalContext.searchUsersInput.disabled = true;
        }
        else {
            modalContext.searchUsersInput.disabled = false;
            modalContext.searchUsersInput.placeholder =
                "Find colleague...";
        }

        modalContext.maxParticipantsNumber.addEventListener("change",
            function participantsNumberChange(event) {
                const eventFull: boolean =
                    modalContext.participantsList.childElementCount - 1 >=
                    modalContext.maxParticipantsNumber.valueAsNumber;

                if (eventFull) {
                    modalContext.searchUsersInput.disabled = true;
                    modalContext.searchUsersInput.placeholder = "Event full!";
                }
                else {
                    modalContext.searchUsersInput.disabled = false;
                    modalContext.searchUsersInput.placeholder = "Find colleague here..";
                }
            });

        modalContext.searchUsersInput.addEventListener("mouseover",
            function searchUsersInputOnMouseOver() {
                CalendarHandler.searchOnFocusEventHandler(modalContext);
            });

        modalContext.searchUsersInput.addEventListener("keyup",
            function searchUsersInputKeyUp(event) {
                CalendarHandler.insertParticipantEventHandler(event, modalContext);
            });

        // @ts-ignore
        $("#exampleModal").on("hidden.bs.modal", function () {
            modalContext.maxParticipantsNumber.value = "";
            modalContext.endTime.value = "";
            const numberOfParticipants = modalContext.participantsList.childElementCount;

            // Clean fetched users 
            if (!!modalContext.usersDataList) {
                (<HTMLDataListElement>modalContext.usersDataList).innerHTML = '';
            }

            // Clean participant list
            const participantsArray = Array.from(modalContext.participantsList.children);
            participantsArray.forEach((participant: HTMLElement) => {
                if (!!participant && !!participant.classList) {
                    if (participant.classList.contains("event-modal__participant")) {
                        participant.parentNode.removeChild(participant);
                    }
                }
            });

        });

        modalContext.modalForm.onsubmit = function submitModalForm(event) {
            event.preventDefault();
            CalendarHandler.createEventHandler(calendarContext, modalContext, calendar);
            return false;
        };

        modalContext.modalForm.onkeydown = function submitModalForm(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
            }
        };

        // @ts-ignore
        $('#exampleModal').modal();
        // Set modal title
        (<HTMLLabelElement>document.getElementById('exampleModalLabel')).textContent = 'Create an Event';
        // // Set event type
        (<HTMLInputElement>document.getElementById('eventTypeId')).value = calendarContext.resource.id;

        // Set number of participants field
        const participantsField = (<HTMLInputElement>document.getElementById('participants'));
        participantsField.disabled = false;
        participantsField.value = calendarContext.resource.extendedProps.minimumParticipants;

        participantsField.setAttribute('min', calendarContext.resource.extendedProps.minimumParticipants);
        // Set max participants
        participantsField.setAttribute('max', calendarContext.resource.extendedProps.maximumParticipants);

        const participantsLabel = <HTMLLabelElement>document.getElementById('participantsLabel');
        participantsLabel.textContent =
            `Participants (${calendarContext.resource.extendedProps.minimumParticipants} - ${calendarContext.resource.extendedProps.maximumParticipants})`;


    }

    private static insertParticipantEventHandler(event, modalContext) {
        const enterPressed: boolean = event.keyCode === 13;
        if (enterPressed) {

            const dataListUserNodes =
                (<HTMLDataListElement>modalContext.usersDataList).children;
            const searchedUsername = modalContext.searchUsersInput.value;
            const nodes = <HTMLInputElement[]>Array.from(dataListUserNodes);

            const userValidEmail = !!nodes.find((node) => {
                return node.value === searchedUsername;
            });

            if (userValidEmail) {
                CalendarHandler.insertParticipant(modalContext);
            }
            else {
                modalContext.searchUsersInput.value = "";
            }

        }
    }

    private static searchOnFocusEventHandler(modalContext: ModalContext) {
        const fillUsersDataList = (users, modal: ModalContext) => {
            users.forEach(function (user) {
                const newOption = document.createElement('option');
                newOption.setAttribute('userId', user.id);
                newOption.classList.add("search__user-select");
                newOption.value = user.email;

                const childOptions: HTMLOptionElement[] =
                    Array.from(modal.usersDataList.children) as HTMLOptionElement[];
                const userInDataList = childOptions.find(option => {
                    return option.value === user.email
                });

                if (!userInDataList) {
                    modal.usersDataList.appendChild(newOption);
                }
            });
        }

        const removeAddedUsersFromDataList = (modal) => {
            var allInputs =
                <HTMLCollectionOf<HTMLInputElement>>document.getElementsByClassName("search__user-select");
            var allParticipants = document.getElementsByClassName("list-group-item");

            for (let x = 0; x < allInputs.length; x++)
                for (let y = 0; y < allParticipants.length; y++)
                    if (allInputs[x].value == allParticipants[y].innerHTML)
                        modal.usersDataList.children[x].remove();

            // CalendarHandler.checkIfNoParticipants(modal);
        }

        CalendarHandler.fetchUsers()
            // .then((users)=>{
            //     cleanUsersDataList(modalContext);
            //     return users;
            // })
            .then((users) => {
                fillUsersDataList(users, modalContext);
            })
            .then(() => {
                removeAddedUsersFromDataList(modalContext);
            })
    }

    private static insertParticipant(modalContext: ModalContext) {
        const { participantsList, searchUsersInput, maxParticipantsNumber, usersDataList } = modalContext;
        const searchBoxFull: boolean = searchUsersInput.value !== "";
        const usersArray = Array.from(usersDataList.options).map(element => {
            const obj = {
                id: element.getAttribute('userId'),
                email: element.value
            };
            return obj;
        });
        if (searchBoxFull) {
            const userToAdd = usersArray.find(element => {
                return element.email === searchUsersInput.value
            });

            // Abort duplicate addition into participants list
            const participantsArray = Array.from(modalContext.participantsList.children);
            const userInList = !!participantsArray.find((participant: HTMLElement) => {
                if (!!participant && !!participant.classList) {
                    if (participant.classList.contains("event-modal__participant")) {
                        return participant.textContent.indexOf(userToAdd.email) !== -1;
                    }
                }
            });

            if (userInList) {
                alert("User already added!");
                searchUsersInput.value = "";
                return;
            }

            // Create participant item and attach it to list beneath search box
            let newParticipantItem = (<HTMLLIElement>document.createElement("li"));
            newParticipantItem.className = "list-group-item";
            newParticipantItem.setAttribute('userId', userToAdd.id);
            newParticipantItem.classList.add("event-modal__participant");
            newParticipantItem.innerHTML = searchUsersInput.value;
            let deleteParticipantSpan = <HTMLSpanElement>document.createElement("span");
            deleteParticipantSpan.innerHTML = "X";
            deleteParticipantSpan.addEventListener("click",
                function deleteParticipant() {
                    this.parentNode.parentNode.removeChild(this.parentNode);

                    // Check if event full 
                    const eventFull: boolean = participantsList.childElementCount - 1 >=
                        maxParticipantsNumber.valueAsNumber;
                    if (eventFull) {
                        searchUsersInput.disabled = true;
                        searchUsersInput.placeholder = "The event is full.";
                    }
                    else {
                        modalContext.searchUsersInput.disabled = false;
                        modalContext.searchUsersInput.placeholder = "Find colleague here..";
                    }
                });

            newParticipantItem.appendChild(deleteParticipantSpan);


            participantsList.appendChild(newParticipantItem);

            searchUsersInput.value = "";

            // Check if event full 
            const eventFull: boolean = participantsList.childElementCount - 1 >=
                maxParticipantsNumber.valueAsNumber;
            if (eventFull) {
                searchUsersInput.disabled = true;
                searchUsersInput.placeholder = "The event is full.";
            }
        }
    };


}
