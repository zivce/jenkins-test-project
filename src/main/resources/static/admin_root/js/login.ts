import Validation from "./utility/validation";
import * as validate from 'validate.js';


const passwordField: HTMLInputElement = <HTMLInputElement>document.getElementById('password');
const eyeField: HTMLElement = document.getElementById('eye');
const loginForm: HTMLElement = document.getElementById("loginForm");
var constraints = {
	email: {
		// Email is required
		presence: true,
		// and must be an email
		format: {
			pattern: "^[A-Za-z0-9._%+-]+@enjoying.rs$",
			message: "domain must be enjoying.rs"
		}
	},
	password: {
		presence: true
	}
};

const submitForm = function (form) {
	// Get from the form
	var values = validate.collectFormValues(form);
	// Validate form against the constraints
	var errors = validate(values, constraints);
	// Update the form 
	Validation.showErrors(form, errors || {});
	// // If no errors send the form
	return !errors;
};

loginForm.onsubmit = function () {
	const thisForm = this;
	return submitForm(thisForm);
};

const togglePasswordVisibility = _ => {
	eyeField.classList.toggle('fa-eye-slash');
	eyeField.classList.toggle('fa-eye');
	passwordField.type = passwordField.type === 'text' ? 'password' : 'text';
};

enum Visibility {
	visible = "visible",
	hidden = "hidden"
}

const hideEyeOnEmptyField = function (fieldValue) {
	if (fieldValue) {
		eyeField.style.visibility = Visibility.visible;
	} else {
		eyeField.style.visibility = Visibility.hidden;
	};
};


passwordField.addEventListener('input', function () {
	hideEyeOnEmptyField(passwordField.value.trim());
});

enum MouseEvents {
	mouseover = 'mouseover',
	mouseout = 'mouseout'
}

eyeField.addEventListener(MouseEvents.mouseover, togglePasswordVisibility);

eyeField.addEventListener(MouseEvents.mouseout, togglePasswordVisibility);

