import { Calendar } from "@fullcalendar/core"
import interaction from "@fullcalendar/interaction";
import resourceTimeGrid from "@fullcalendar/resource-timegrid";
import { OptionsInput } from "@fullcalendar/core";
import CalendarHandler from "./calendar-handler";
import ModalContext from "./models/modal-context";

// Prevent letter input in input type Number
//@ts-ignore	
$("[type='number']").keyup(function (evt) {
	evt.preventDefault();
});

//@ts-ignore
$(document).ready(function () {
	const calendarEl = document.getElementById('calendar');
	const calendarConfig: OptionsInput = {
		slotDuration: '00:10:00',
		minTime: '08:00:00',
		maxTime: '19:00:00',
		allDaySlot: false,
		locale: 'en-gb',
		schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
		plugins: [interaction, resourceTimeGrid],
		selectable: true,
		defaultView: 'resourceTimeGridDay',
		header: {
			left: 'prev,next today',
			center: 'title',
			right: ''
		},
		nowIndicator: true,
		eventOverlap: false,
		selectOverlap: false,
		slotEventOverlap: true,
		resources: CalendarHandler.fetchResources,
		selectAllow: CalendarHandler.selectTimeSlots,
		select: (calendarContext) => {
			const now = new Date();

			const selectedTime = new Date(calendarContext.start);

			// If timeDiff is positive open modal
			const timeDiff = <any>selectedTime - <any>now;

			if (timeDiff < 0) {
				alert('You cannot change the past :( !!!');
				return;
			}

			const searchUsersInput: HTMLInputElement =
				<HTMLInputElement>document.getElementById('searchUsersInput');
			const usersDataList =
				<HTMLDataListElement>document.getElementById('users-datalist');
			const maxParticipantsNumber =
				(<HTMLInputElement>document.getElementById("participants"));
			const endTime =
				(<HTMLInputElement>document.getElementById("endTime"));
			const participantsList =
				(<HTMLUListElement>document.getElementById("participantsList"));
			const selectedListItem =
				(<HTMLLIElement>document.createElement("li"));
			const helpBlock = document.getElementById("attendanceHelpBlock");
			const modalForm = <HTMLFormElement>document.getElementById("modalForm");
			const startTime = (<HTMLInputElement>document.getElementById('startTime'));
			const currentDate = document.getElementsByTagName('h2')[0].innerHTML;
			const eventTypeId = (<HTMLInputElement>document.getElementById('eventTypeId'));

			const modalContext: ModalContext = {
				searchUsersInput,
				usersDataList,
				maxParticipantsNumber,
				endTime,
				participantsList,
				selectedListItem,
				helpBlock,
				modalForm,
				startTime,
				currentDate,
				eventTypeId,
			}
			CalendarHandler.calendarSelect(calendarContext, modalContext, calendar);
		},

		// TODO: extract to separate function and pass by reference
		eventClick: (calendarContext) => {
			const searchUsersInput: HTMLInputElement =
				<HTMLInputElement>document.getElementById('searchUsersInput');
			const usersDataList =
				<HTMLDataListElement>document.getElementById('users-datalist');
			const maxParticipantsNumber =
				(<HTMLInputElement>document.getElementById("participants"));
			const endTime =
				(<HTMLInputElement>document.getElementById("endTime"));
			const participantsList =
				(<HTMLUListElement>document.getElementById("participantsList"));
			const selectedListItem =
				(<HTMLLIElement>document.createElement("li"));
			const helpBlock = document.getElementById("attendanceHelpBlock");
			const modalForm = <HTMLFormElement>document.getElementById("modalForm");
			const startTime = (<HTMLInputElement>document.getElementById('startTime'));
			const currentDate = document.getElementsByTagName('h2')[0].innerHTML;
			const eventTypeId = (<HTMLInputElement>document.getElementById('eventTypeId'));

			const modalContext: ModalContext = {
				searchUsersInput,
				usersDataList,
				maxParticipantsNumber,
				endTime,
				participantsList,
				selectedListItem,
				helpBlock,
				modalForm,
				startTime,
				currentDate,
				eventTypeId,
			}
			CalendarHandler.eventClickHandler(calendarContext, modalContext);
		}
	}

	const calendar = new Calendar(calendarEl, calendarConfig);
	calendar.render();
	CalendarHandler.addEvents(calendar);

});




