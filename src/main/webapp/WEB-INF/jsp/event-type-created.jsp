<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title> Cart </title>
    <link rel="stylesheet" href="./styles/designCET.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="/admin_root/dist/css/background.css" rel="stylesheet" type="text/css">
</head>
<body>
   <div class="main">
   <div class="container">
    <h2 class="text-event">You have successfully <c:if test = "${param.id != null}">
         			    edited a type of events!
      				</c:if> 
      				<c:if test = "${param.id == null}">
      					 created a new type of events!
      				</c:if>
      				</h2>
    
    <h3 class="text-event">Please wait</h3>
    </div></div>
    <script>
  	setTimeout(function() {
      document.location = "/admin/event-type-list";
 		 }, 1500); // <-- this is the delay in milliseconds
	</script>
    <%--
    String redirectURL = "http://enjoying.rs";
    response.sendRedirect(redirectURL);
	--%>
    
</body>
</html>