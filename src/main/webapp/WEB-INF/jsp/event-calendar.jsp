<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>
  <meta charset="ISO-8859-1">
  <title>Event Calendar</title>
	<link href="../webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	
	<link href="/admin_root/dist/css/event-calendar.css" rel="stylesheet" type="text/css">
	<link href="/admin_root/dist/css/navigation.css" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="../webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
	<script src="/admin_root/dist/js/event-calendar.js" defer></script>

  <script>
    fetch('http://localhost:8080/api/users/current')
      .then(function (response) {
          return response.json();
      }) 
      .then(function (user) {
          window.client = {};
          client.id = user.id;
          client.email = user.email;
      }) 
  </script>
</head>

<body>

  <%@ include file="common/navigation.jspf" %>
  <div id='calendar'></div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form id="modalForm" method='post'>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <input type="hidden" id="eventTypeId" name="eventTypeId" value="0" />
        
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 id="exampleModalLabel" class="modal-title" id="exampleModalLabel">Create an event</h5>
          </div>
          <div class="modal-body">

            <div class="container-fluid">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="eventTypeName" class="control-label">Type of event</label>
                    <input name="eventTypeName" id="eventTypeName" type="text" class="form-control" disabled>
                  </div>

                  <div class="form-group">
                    <label for="participants" class="control-label">Number of participants</label>
                    <input name="numberOfParticipants" id="participants" type="number" class="form-control" disabled>
                  </div>                  

                  <div class="form-group">
                    <label for="startTime" class="control-label">Starting time</label>
                    <div class="input-group clockpicker">
                      <input name="startDate" id="startTime" type="text" class="form-control" value="09:30" disabled>
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                      </span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="endTime" class="control-label">Ending time</label>
                    <div class="input-group clockpicker">
                      <input name="endDate" id="endTime" type="text" class="form-control" value="10:00" disabled>
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                      </span>
                    </div>
                  </div>

                </div>
                <div class="col-md-6">
                  <label id="participantsLabel" for="participantsList">Participants</label>

                  <ul id="participantsList" class="list-group"
                    style="max-height:220px !important; overflow: auto !important;">

                    <li class="list-group-item">
                      <input type="text" class="form-control" id="searchUsersInput" list="users-datalist"
                        placeholder="Start typing a name to add a colleague">
                      <datalist id="users-datalist"></datalist>
                    </li>

                  </ul>


                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer"><button id="deleteButton" type="button" class="btn btn-danger pull-left"
              data-dismiss="modal">Delete event</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button id="submitButton" type="submit" class="btn btn-primary">Create an event</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  	<script src="/admin_root/dist/js/navigation.js"></script>
</body>

</html>