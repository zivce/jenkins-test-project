<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Login</title>
	<link href="../webjars/bootstrap/3.2.0/css/bootstrap.min.css"
		rel="stylesheet" type="text/css">	
	<link href="/admin_root/dist/css/error-page.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="content">		
		<div>
			<p class="status"><span class="status__code">${code}</span><span class="status__message">.${message}</span></p>
			<p class="status status__description">${description}</p>
			<button type="button" class="btn btn-default center-block" onclick="window.history.back()">Go Back</button>		
		</div>
	</div>
</body>
</html>