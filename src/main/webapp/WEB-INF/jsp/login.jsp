<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link href="../webjars/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
	integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
	crossorigin="anonymous">
	<link href="/admin_root/dist/css/background.css" rel="stylesheet" type="text/css">
<link href="/admin_root/dist/css/login_form.css" rel="stylesheet" type="text/css">

</head>
<body style="text-align: center;">
	<div class="main"> 
	<div class="container">
	
		<p class="">${registerMessage}</p>

		<form id="loginForm" class="form-signin needs-validation"
			method="POST" action="doLogin" novalidate>
			<h2 class="form-heading">Log in</h2>

			<div class="form-group ${errorEmail != null ? 'has-error' : ''}">
				<input id="email" name="email" type="text" class="form-control"
					placeholder="Email" autofocus required /> <span id="errorEmail"
					class="help-block ${errorEmail != null ? 'with-errors' : ''}">${errorEmail}</span>
			</div>
			<div class="form-group ${errorPassword != null ? 'has-error' : ''}">
				<input id="password" name="password" type="password"
					class="form-control" placeholder="Password" required />
				<i id="eye"	class="fa fa-fw fa-eye field-icon"></i>
				<span class="help-block ${errorPassword != null ? 'with-errors' : ''}">${errorPassword}</span>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<button id="submitButton" type="submit" class="btn btn-lg btn-block"
				onClick="">Log In</button>
			<h4 class="text-center" >
				<a href="/register.html" style="color:black; float:left;">Registration</a>
				<a href="/forgotPassword" style="color:black; float:right;">Forgot password</a>
			</h4>
		</form>
	</div></div>
	<script src="/admin_root/dist/js/login.js"></script>
</body>
</html>